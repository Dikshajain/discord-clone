import firebase from "firebase";


const firebaseConfig = {
    apiKey: "AIzaSyB5jcS9061vy8EAjDMhVWvkJC578gcRaGU",
    authDomain: "discord-cl-d0ce0.firebaseapp.com",
    projectId: "discord-cl-d0ce0",
    storageBucket: "discord-cl-d0ce0.appspot.com",
    messagingSenderId: "903459568841",
    appId: "1:903459568841:web:d43c90254f7843a0372118"
  };

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db=firebaseApp.firestore();
const auth=firebase.auth();
const provider=new firebase.auth.GoogleAuthProvider();
export{auth,provider};
export default db;