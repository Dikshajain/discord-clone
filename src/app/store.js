import { configureStore } from '@reduxjs/toolkit';
import { composeWithDevTools } from 'redux-devtools-extension';
import userReducer from '../features/userSlice';
import appReducer from '../features/appSlice';



export const store = configureStore({
  reducer: {
    user: userReducer,
    app:  appReducer,
   
  },
});
