import React,{useState,useEffect} from 'react'
import './Chat.css';
import ChatHeader from './ChatHeader';
import Message from './Message';
import AddCircleIcon from  '@material-ui/icons/AddCircle';
import  EmojiEmotionsIcon  from '@material-ui/icons/EmojiEmotions';
import GifIcon from '@material-ui/icons/Gif';
import CardGiftcardIcon from '@material-ui/icons/CardGiftcard';
import { useSelector } from 'react-redux';
import { selectUser } from './features/userSlice';
import firebase from 'firebase';
import { selectChannelId, selectChannelName } from './features/appSlice';
import db from './firebase';
function Chat() {
    const[input,setInput]=useState("");
    const[messages,setMessages]=useState([]);
    const user=useSelector(selectUser);
    const channelId=useSelector(selectChannelId);
    const channelName=useSelector(selectChannelName);

   useEffect(() => {
       if(channelId){
       db.collection("channels").doc(channelId).collection("messages").orderBy("timestamp","desc").onSnapshot((snapshot)=>
       setMessages(snapshot.docs.map((doc)=>doc.data())))
       }
   }, [channelId])

    const submit=(e)=>{
        e.preventDefault();
        db.collection("channels").doc(channelId).collection("messages").add({
           message:input,
           user:user,
           timestamp:firebase.firestore.FieldValue.serverTimestamp(),
        })
        setInput("");
    }

   
    return (
        <div className="chat">
            <ChatHeader channelName={channelName}/>

            <div className="chat__messages">
               {messages.map((message)=>(
                   <Message timestamp={message.timestamp} message={message.message} user={message.user}/>
               ))}
                


            </div>
            <div className="chat__input">
                <AddCircleIcon fontsize="large"/>
                
                <form>
                    <input placeholder={`Message #${channelName}`}  value={input} onChange={(e)=>setInput(e.target.value)} />
                    <button type="submit" onClick={submit} disabled={!channelId}>Send Message</button>
                </form>

                <div className="chat__inputIcons">
                    <CardGiftcardIcon fontSize="large"/>
                    <GifIcon fontSize="large"/>
                    <EmojiEmotionsIcon fontSize="large"/>
                </div>
            </div>
        </div>
    )
}

export default Chat
