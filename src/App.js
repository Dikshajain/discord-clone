import React,{useEffect} from 'react';
import {auth} from './firebase';
import {selectUser} from './features/userSlice';
import {useSelector,useDispatch} from 'react-redux';

import Login from './Login';
import {login,logout} from './features/userSlice';
import './App.css';
import Sidebar from './Sidebar';
import Chat from './Chat';



function App() {
  const user=useSelector(selectUser);
  const dispatch=useDispatch();


  useEffect(() => {
    auth.onAuthStateChanged((authUser)=>{
      console.log("user is",authUser);
        if(authUser){
          //the user is logged in
          dispatch(
            login({
              uid:authUser.uid,
              photo:authUser.photoURL,
              email:authUser.email,
              displayName:authUser.displayName,
            })
          )
        }
        else{
            dispatch(logout());
        }
      })
    
  },[dispatch])
 
  
  return (
    <div className="app">
      {!user ? 
        (<Login />)
        :
    
        (
          <>
        <Sidebar/>
        <Chat />
        </>

        )}



      )
    </div>
  );
}

export default App;
